package es.progcipfpbatoi;

import es.progcipfpbatoi.controlador.*;
import javafx.application.Application;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App extends Application
{
    @Override
    public void start(Stage stage) throws IOException {

        Initializable maincontroller = new MainController();
        ChangeScene.change(stage, maincontroller, "/vista/main.fxml");
    }

    public static void main(String[] args) {
        launch();
    }
}
