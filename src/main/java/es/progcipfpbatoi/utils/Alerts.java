package es.progcipfpbatoi.utils;

import javafx.scene.control.Alert;

public class Alerts {

    public void mostrarAlert(String mensajeError, Alert.AlertType tipoAlerta) {
        Alert alert = new Alert(tipoAlerta);
        alert.setHeaderText(null);
        alert.setTitle("Alerta");
        alert.setContentText(mensajeError);
        alert.showAndWait();
    }
}
