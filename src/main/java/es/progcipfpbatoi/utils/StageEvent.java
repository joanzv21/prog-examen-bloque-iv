package es.progcipfpbatoi.utils;

import javafx.scene.Node;
import javafx.stage.Stage;

import java.util.EventObject;

public class StageEvent {

    public static Stage getStageFromEvent(EventObject event) {
        return (Stage) ((Node) ((EventObject) event).getSource()).getScene().getWindow();
    }
}
