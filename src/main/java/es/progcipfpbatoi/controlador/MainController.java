package es.progcipfpbatoi.controlador;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    @FXML
    private Label labelTotalPisos;

    @FXML
    private Label labelTotalVisitas;

    public MainController() {

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}

