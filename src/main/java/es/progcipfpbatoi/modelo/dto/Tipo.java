package es.progcipfpbatoi.modelo.dto;

public enum Tipo {
    VPO, LIBRE;

    public static Tipo toTipo(String tipo) {
        if (tipo.equals("VPO")) {
            return VPO;
        } else if (tipo.equals("LIBRE")) {
            return LIBRE;
        }

        return null;
    }
}
